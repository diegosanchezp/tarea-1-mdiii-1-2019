#    ---Tarea 1 MDII---
# Realizada por Diego Sánchez
#         26334929
#   Ejecutar con python3.6

import pdb  # Debbuger
import sys  # Para obtener el numero entero mas grande
INFINITO = sys.maxsize
#---------Estructuras de datos---------#


class Vertice:
  def __init__(self, key):
    """Constructor"""
    self.id = key
    self.conectadoA = {}

  def anadirVecino(self, nbr, weight):
    """Anadir un vertice vecino"""
    self.conectadoA[nbr] = weight

  def __str__(self):
    return str(self.id) + ' :conectadoA ' + str([x.id for x in self.conectadoA])

  def getConnections(self):
    """Obtener los vecinos del vertice, retorna objetos vertices"""
    return self.conectadoA.keys()

  def getTiempo(self, nbr):
    """Obtener el tiempo y peso que hay entre dos aristas
    retorna par (t,p) donde t es el tiempo y p el peso maximo"""
    return self.conectadoA[nbr]


class Grafo:
  def __init__(self):
    self.vertList = {}
    self.numVertices = 0

  def anadirVertice(self, key):
    self.numVertices = self.numVertices + 1
    newVertice = Vertice(key)
    self.vertList[key] = newVertice
    return newVertice

  def getVertex(self, n):
    if n in self.vertList:
        return self.vertList[n]
    else:
        return None

  def __contains__(self, n):
    return n in self.vertList

  def anadirArista(self, f, t, cost):
    if f not in self.vertList:
        self.anadirVertice(f)
    if t not in self.vertList:
        self.anadirVertice(t)

    self.vertList[f].anadirVecino(self.vertList[t], cost)
    self.vertList[t].anadirVecino(self.vertList[f], cost)

  def getVertices(self):
    # Los keys() de vertList son los id de los vertices
    return self.vertList.keys()

  def __iter__(self):
    # Los values() de vertList son objetos vertices
    return iter(self.vertList.values())


class Columna:
  def __init__(self, v, d):
    """Constructor"""
    self.c = {}
    self.c["vertice"] = v  # Puntero a objeto vertice
    self.c["verticeID"] = v.id
    self.c["menorTiempo"] = d
    self.c["vPrevio"] = []

  def setDistancia(self, d):
    self.c["menorTiempo"] = d

  def addPrevio(self, v):
    """Anadir vertice previo"""
    self.c["vPrevio"].append(v)

  def imprimir(self):
    """Imprimir informacion que contiene la columna solo para debuggin"""
    print("----")
    print(f'Vertice: {self.c["verticeID"]}')
    print(f'Tiempo mas corto {self.c["menorTiempo"]}')
    print(f'Vertices previos {[v for v in self.c["vPrevio"]]}')


class Tabla:
  def __init__(self):
    self.columnas = []

  def anadirColumna(self, c):
    """Anadir una columna a la tabla"""
    self.columnas.append(c)

  def menorTiempo(self, Q):
    """Obtener el vertice con el menor tiempo"""
    col_temp = []
    for v in Q:
      for c in self.columnas:
        if v == c.c["verticeID"]:
          col_temp.append(c)
          break

    minimo = col_temp[0].c["menorTiempo"]
    u = col_temp[0].c["vertice"]
    for c in col_temp:
      if c.c["menorTiempo"] < minimo:
        minimo = c.c["menorTiempo"]
        u = c.c["vertice"]
    return u

  def tiempo(self, vID):
    """Retorna el menor tiempo asociado a un vertice"""
    for c in self.columnas:
      if c.c["verticeID"] == vID:
        return c.c["menorTiempo"]

  def setTiempo(self, vID, t):
    """Asignar el tiempo a un vertice"""
    for c in self.columnas:
      if c.c["verticeID"] == vID:
        c.c["menorTiempo"] = t
        return

  def setvPrevio(self, vID, vPID):
    """Asignar vertice previo"""
    for c in self.columnas:
      if c.c["verticeID"] == vID:
        c.c["vPrevio"].append(vPID)
        return

  def imprimir(self):
    """Imprimir informacion de la tabla solo para debugging"""
    for c in self.columnas:
      c.imprimir()

  def helper(self, vID):
    """Ayuda a obtener el camino mas corto solo para uso interno de la clase"""
    for c in self.columnas:
      if c.c["verticeID"] == vID:
        p = c.c["vPrevio"]
        return p

  def camino_corto(self, vDestino):
    """Obtiene el camino mas corto"""
    p = self.helper(vDestino)
    path = []
    path.insert(0, vDestino)
    while True:
      try:
        if len(p) == 0:
          break
      except TypeError:
        return
      else:
        path.insert(0, p[-1])
        p = self.helper(p[-1])
    return path

#---------Algoritmo de Dijkstra---------#


def Dijkstra(grafo, v_inicio):
  # Crear tabla donde se guarda la informacion producida por el algoritmo
  T = Tabla()
  for v in grafo:
    if v.id == v_inicio:
      c = Columna(v, 0)
    else:
      c = Columna(v, INFINITO)
    # Anadir c a Tabla
    T.anadirColumna(c)

  Q = list(g.getVertices())
  # Mientras que Q no este vacio
  while len(Q) != 0:
    # Obterner vertice u con el menor tiempo en la tabla
    u = T.menorTiempo(Q)
    # Eliminar a u de Q
    Q.remove(u.id)
    for vecino in u.getConnections():
      if T.tiempo(u.id) + u.getTiempo(vecino)[0] < T.tiempo(vecino.id):
        T.setTiempo(vecino.id, T.tiempo(u.id) + u.getTiempo(vecino)[0])
        T.setvPrevio(vecino.id, u.id)
  return T


#---------Entra estandar---------#

N = int(input())

for X in range(1, N + 1):
  peso_camion = 0
  tiempo_total = 0
  P_C = {}
  orden_visita = []
  aristas_orignal = []
  camino_camion = []

  while True:

    temp = input().split()
    # Leer pares P C
    if len(temp) == 2:
      P_C[int(temp[0])] = int(temp[1])
      orden_visita.append(int(temp[0]))
      peso_camion += int(temp[1])
    # Leer Enteros O D T Pm
    elif len(temp) == 4:
      temp = [int(i) for i in temp]
      # Construir lista de aristas_orignal
      aristas_orignal.append(temp)
    if len(temp) == 1:
      if temp[0] == "":
        break
      else:
        # A partir de aqui ya se leyo todo el caso
        I = int(temp[0])
        for i in range(len(orden_visita) - 1):
          # Eliminar el primer vertice en la lista orden_visita
          if i == 0:
            if orden_visita[0] == I:
              I = orden_visita.pop(0)
          elif i > 0:
              I = orden_visita.pop(0)
          # Restar a peso camion el peso de mercancia asociado a pv
          peso_camion -= P_C[I]
          # Dado el peso del camion eliminar aristas inaccesibles
          copia_aristas = []
          for arista in aristas_orignal:
            if arista[3] >= peso_camion:
              copia_aristas.append(arista)
          # Construir grafo sin las arista inaccesibles
          g = Grafo()
          for arista in copia_aristas:
            g.anadirArista(arista[0], arista[1], (arista[2], arista[3]))
          # Aplicar algorimo de Dijkstra para vertice I
          T = Dijkstra(g, I)
          # Guardar el camino recorrido por el camion
          short = T.camino_corto(orden_visita[0])
          if short is not None:

            if i > 0:
              short.pop(0)
            for v in short:
              camino_camion.append(v)
            # Guardar y acumular el tiempo
            tiempo_total += T.tiempo(orden_visita[0])
        # A partir de aqui es la salida
        print(f'Caso {X}:')
        print(" ".join(str(i) for i in camino_camion))
        print(tiempo_total)
        print("")
        break

"""
2
1 2
4 4
2 10
3 4
1 2 3 18
1 4 8 25
1 3 3 10
2 3 5 10
2 4 4 20
3 4 2 18
1

1 5
2 5
3 5
1 2 2 1
1 3 2 1
1

 """

"""
Tabla = [
  {"vertice": 1,
  "distCorta": 1,
  "verticePrevio": []
  },
  {"verticeID": 2,
  "distCorta": INFINITO,
  "verticePrevio": []
  }
]

K = {"verticeID": 1, "distCorta": 1, "verticePrevio": []}
"""
